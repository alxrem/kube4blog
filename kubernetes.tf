variable "api_token" {}

variable "server_names" {
  default = [
    "master1",
    "node1",
    "node2",
    "node3"
  ]
}

provider "hcloud" {
  token = "${var.api_token}"
}

resource "hcloud_ssh_key" "default" {
  name       = "admin"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "hcloud_server" "kubernetes" {
  count       = "4"

  name        = "${var.server_names[count.index]}"
  image       = "ubuntu-16.04"
  server_type = "cx11"
  ssh_keys    = ["admin"]
  depends_on  = ["hcloud_ssh_key.default"]
}

resource "null_resource" "cluster" {
  triggers = {
    ids = "${join(",", hcloud_server.kubernetes.*.id)}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i terraform-hetzner-inventory bootstrap.yml"
  }
}
