#!/usr/bin/env python
# vim: ts=4 sts=4 sw=4 et fileencoding=utf-8


def id_to_name(getent_data, gid):
    for name, params in getent_data.iteritems():
        if int(params[1]) == gid:
            return name
    return None


class FilterModule(object):
    def filters(self):
        return dict(id_to_name=id_to_name)
